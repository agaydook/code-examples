# frozen_string_literal: true

class EnrollmentBuilder::EnrollmentForm < EnrollmentBuilder::BaseForm
  attribute :provider

  attribute :name, String
  attribute :learning_opportunity_id, String
  attribute :status_id, String
  attribute :start_date, String
  attribute :end_date, String
  attribute :duration, Integer
  attribute :available_sits, Integer
  attribute :application_deadline, String
  attribute :language_ids, Array

  validates :name, :learning_opportunity_id, :status_id, :language_ids, :start_date, :provider, presence: true
  validate :lo_related_to_provider?

  def language_ids=(ids)
    return if ids.blank?

    super ids.delete_if(&:blank?)
  end

  def persist!
    manage_enrollment!
  end

  private

  def manage_enrollment!
    @enrollment ||= Enrollment.new

    @enrollment.assign_attributes(form_params)

    @enrollment.save!
  end

  def lo_related_to_provider?
    lo = provider.learning_opportunities.find(learning_opportunity_id)

    errors.add :learning_opportunity_id, 'should be related with provider' if lo.blank?
  end
end

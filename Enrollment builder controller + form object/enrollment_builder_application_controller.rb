# frozen_string_literal: true

class Api::V1::EnrollmentBuilder::EnrollmentBuilderApplicationController < Api::V1::ApplicationController
  include ApiConcern
  include ProviderUserConcern

  before_action :load_enrollment

  def update
    enrollment_builder_form = form.new(
      form_params.merge(
        enrollment: @enrollment,
        provider: provider_user.provider
      )
    )

    enrollment_builder_form.save!

    render json: enrollment_builder_form.enrollment,
           serializer: ::EnrollmentBuilder::EnrollmentSerializer,
           include: include_schema
  end

  private

  def form
    raise NotImplementedError
  end

  def form_params
    raise NotImplementedError
  end

  def load_enrollment
    @enrollment = provider_user.provider
                               .enrollments
                               .find(params[:id])
  end

  def include_schema
    ['*', enrollment_modules: %i[lecturer address]]
  end
end

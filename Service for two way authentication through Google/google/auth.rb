# frozen_string_literal: true

class Google::Auth < BaseService
  def call
    validate_context

    auth
  end

  private

  def validate_context
    raise ArgumentError if auth_data.blank?
    raise ArgumentError, I18n.t('auth.google.errors.undefined_code') if auth_data[:code].blank?
  end

  def auth
    context.user_info = user_info
    context.pretty_user_info = pretty_user_info
  end

  def user_info
    @user_info ||= receive_profile_data.parsed
  end

  def client
    @client ||= OAuth2::Client.new(
      ENV['GOOGLE_CLIENT_ID'],
      ENV['GOOGLE_CLIENT_SECRET'],
      token_url: 'https://www.googleapis.com/oauth2/v4/token'
    )
  end

  def token
    @token ||= client.auth_code.get_token(
      auth_data[:code],
      redirect_uri: ENV['GOOGLE_AUTH_REDIRECT_URL']
    )
  end

  def auth_data
    context.auth_data
  end

  def receive_profile_data
    token.get('https://www.googleapis.com/oauth2/v2/userinfo')
  end

  def pretty_user_info
    {
      email: user_info['email'],
      first_name: user_info['given_name'],
      last_name: user_info['family_name'],
      auth_provider: true,
      verified_email: user_info['verified_email'],
      avatar_url: user_info['picture']
    }
  end
end

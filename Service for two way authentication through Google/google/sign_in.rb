# frozen_string_literal: true

class Google::SignIn < BaseService
  def call
    validate_context

    sign_in
  end

  private

  def validate_context
    raise ArgumentError if auth_data.blank?
  end

  def sign_in
    result = Google::Auth.call(auth_data: auth_data)

    raise StandardError, result.error unless result.success?

    pretty_user_info = result.pretty_user_info
    context.user = User.find_by(email: pretty_user_info[:email]) || pretty_user_info.merge!(auth_provider: true)
  end

  def auth_data
    context.auth_data
  end
end

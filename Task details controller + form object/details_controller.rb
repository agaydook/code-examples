# frozen_string_literal: true

class Admin::Task::DetailsController < ApplicationController
  before_action :load_task

  def edit
    @task_details = details_form.new(task: @task)
  end

  def update
    @task_details = details_form.new(task_form_params)

    if @task_details.save
      # TODO: redirect to tasks index page.
      redirect_to edit_admin_task_details_path(@task_details.task)
    else
      render :edit, status: :unprocessable_entity
    end
  end

  private

  def task_form_params
    params.require(:task_details).permit(*retrieve_schema_params).merge(task: @task)
  end

  def load_task
    @task = Task.find(params[:task_id])
  end

  def details_form
    @task.type.constantize.const_get('Form')
  end

  def retrieve_schema_params
    details_form.schema_params
  end
end

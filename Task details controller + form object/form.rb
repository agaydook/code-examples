# frozen_string_literal: true

class Task::OptionTask::Form < ::ApplicationForm
  PARAMS_SCHEMA = [
    :instruction,
    :introduction,
    :audio,
    task_items_attributes: [
      :id,
      :sentence,
      :_destroy,
      task_item_options_attributes: %i[
        id
        text_option
        correct
        _destroy
      ]
    ]
  ].freeze
  ITEM_OPTION_FORM_CLASS = Task::OptionTask::ItemOptionForm
  ITEM_FORM_CLASS = Task::OptionTask::ItemForm

  attribute :task, Task

  attribute :instruction, String, default: ->(form, _attribute) { form.task.instruction }
  attribute :introduction, String, default: ->(form, _attribute) { form.task.introduction }
  attribute :audio, ActiveStorage::Attached::One
  attribute :task_items, Array[ITEM_FORM_CLASS], default: :set_default_items
  attribute :nested_attrs

  validates_presence_of :introduction
  validate :validate_task_items

  def initialize(args = {})
    raise ArgumentError, "Task can't be blank" unless args.fetch(:task, nil)
    
    super
  end

  def task_items_attributes=(attributes)
    @task_items_params = attributes.values
    @task_items = @task_items_params.reject { |v| v['_destroy'] == '1' }.map { |v| self.class::ITEM_FORM_CLASS.new(v) }
  end

  def build_task_items
    self.class::ITEM_FORM_CLASS.new
  end

  def self.schema_params
    PARAMS_SCHEMA
  end

  private

  def validate_task_items
    errors.add(:base, 'Form is invalid') if @task_items.map(&:valid?).include? false
  end

  def persist!
    update_task!
  end

  def update_task!
    @task.assign_attributes(instruction: @instruction, introduction: @introduction, task_items_attributes: @task_items_params)
    @task.audio = @audio if @audio.present?
    @task.save!
  end

  def set_default_items
    @task.task_items.present? ? items_from_task : [self.class::ITEM_FORM_CLASS.new]
  end

  def items_from_task
    @task.task_items.map do |item|
      self.class::ITEM_FORM_CLASS.new(
        id: item.id,
        sentence: item.sentence,
        task_item_options: options_from_item(item)
      )
    end
  end

  def options_from_item(item)
    item.task_item_options.map do |item_option|
      self.class::ITEM_OPTION_FORM_CLASS.new(
        id: item_option.id,
        correct: item_option.correct,
        text_option: item_option.text_option
       )
    end
  end
end

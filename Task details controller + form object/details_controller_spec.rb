# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Admin::Task::DetailsController, type: :controller do
  let!(:select_task) { build_stubbed(:select_task) }
  let(:select_task_form) { instance_double(Task::SelectTask::Form) }
  let(:valid_params) do
    {
      introduction: 'Introduction',
      task_items_attributes: {
        '0': {
          sentence: 'Sentence *select*',
          task_item_options_attributes: {
            '0': {
              text_option: 'Title',
              correct: true
            }
          }
        }
      }
    }
  end
  let(:invalid_params) { valid_params.merge(introduction: nil) }
  let(:stub_task_find) { allow(Task).to receive(:find).with(select_task.id.to_s).and_return(select_task) }
  let(:stub_form) do
    allow(select_task_form).to receive(:task).and_return(select_task)

    allow(Task::SelectTask::Form).to receive(:new).and_return(select_task_form)
  end

  describe 'GET #edit' do
    before do
      stub_task_find
      get :edit, params: { task_id: select_task.id }
    end

    it { expect(assigns(:task_details)).to be_truthy }
    it { is_expected.to respond_with(200) }
    it { is_expected.to render_template(:edit) }
  end

  describe 'PATCH #update' do
    context 'with valid params' do
      before do
        stub_task_find
        stub_form
        allow(select_task_form).to receive(:save).and_return(true)

        patch :update, params: { task_details: valid_params, task_id: select_task.id }
      end

      it { expect(assigns(:task_details)).to be_truthy }
      it { is_expected.to respond_with(302) }
    end

    context 'with invalid params' do
      before do
        stub_task_find
        stub_form
        allow(select_task_form).to receive(:save).and_return(false)

        patch :update, params: { task_details: invalid_params, task_id: select_task.id }
      end

      it { expect(assigns(:task_details)).to be_truthy }
      it { is_expected.to respond_with(422) }
      it { is_expected.to render_template(:edit) }
    end
  end
end

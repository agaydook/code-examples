# frozen_string_literal: true

class LO::UpdateRatingData < BaseService
  DATA_ITEM = Struct.new(:id, :rating)
  SUMMARY_DATA_ITEM = Struct.new(:rating, :reviews_count)
  QUERY_SELECT = 'percentile_disc(0.5) within group (order by score) as rating'

  def call
    raise ArgumentError if learning_opportunity.blank?

    update_rating_data!
  end

  private

  def update_rating_data!
    learning_opportunity.update!(rating_data: collect_rating_data)
  end

  def collect_rating_data
    {
      impact_levers_summary: summary_calculate_impact_levers_data,
      skills_summary: summary_calculate_skills_data,
      impact_levers: calculated_impact_levers_data,
      skills: calculated_skills_data
    }
  end

  def calculated_impact_levers_data
    @calculated_impact_levers_data ||= rating_data(review_impact_levers_collection)
  end

  def calculated_skills_data
    @calculated_skills_data ||= rating_data(review_skills_collection)
  end

  def summary_calculate_impact_levers_data
    rating = median_rating(calculated_impact_levers_data)
    reviews_count = learning_opportunity.reviews
                                        .joins('RIGHT JOIN review_impact_scores ON review_impact_scores.review_id = reviews.id')
                                        .distinct
                                        .count

    SUMMARY_DATA_ITEM.new(rating, reviews_count).to_h
  end

  def summary_calculate_skills_data
    rating = median_rating(calculated_skills_data)
    reviews_count = learning_opportunity.reviews
                                        .joins('RIGHT JOIN review_skill_scores ON review_skill_scores.review_id = reviews.id')
                                        .distinct
                                        .count

    SUMMARY_DATA_ITEM.new(rating, reviews_count).to_h
  end

  def review_impact_levers_collection
    ReviewImpactScore.select(QUERY_SELECT)
                     .select('impact_lever_id as item_id')
                     .where(review_id: actual_review_ids)
                     .group('item_id')
  end

  def review_skills_collection
    ReviewSkillScore.select(QUERY_SELECT)
                    .select('skill_id as item_id')
                    .where(review_id: actual_review_ids)
                    .group('item_id')
  end

  def rating_data(collection)
    collection.map do |item|
      DATA_ITEM.new(item.item_id, item.rating).to_h
    end
  end

  def median_rating(collection)
    collection.map do |data|
      data[:rating]
    end.median
  end

  def learning_opportunity
    context.learning_opportunity
  end

  def actual_review_ids
    @actual_review_ids ||= learning_opportunity.reviews
                                               .actual
                                               .pluck(:id)
  end
end

# frozen_string_literal: true

require 'rails_helper'

describe LO::UpdateRatingData do
  # Score also is used as count of related records to review
  let(:score) { 5 }
  let!(:enrollment) { create(:enrollment) }
  let!(:review) do
    create(:review_with_skills_and_impacts,
           enrollment: enrollment,
           completed: true,
           current: true,
           records_count: score)
  end
  let!(:learning_opportunity) { review.learning_opportunity }

  context 'when update LO rating data' do
    context 'with valid params' do
      subject(:result) { described_class.call(learning_opportunity: learning_opportunity) }

      it 'returns empty rating_data before calculation' do
        expect(learning_opportunity.rating_data.blank?).to be true
      end

      it 'returns rating_data after calculation' do
        expect(result.learning_opportunity.rating_data.blank?).to be false
      end

      it "matches review's soft skills data to rating_data" do
        rating_data = result.learning_opportunity.rating_data

        expect(rating_data['soft_skill'].present?).to be true
        expect(rating_data['soft_skill']['rating']).to eq score
        expect(rating_data['soft_skill']['reviews_count']).to eq review.review_skill_scores.soft.count
      end

      it "matches review's hard skills data to rating_data" do
        rating_data = result.learning_opportunity.rating_data

        expect(rating_data['hard_skill'].present?).to be true
        expect(rating_data['hard_skill']['rating']).to eq score
        expect(rating_data['hard_skill']['reviews_count']).to eq review.review_skill_scores.hard.count
      end

      it "matches review's impact levers data to rating_data" do
        rating_data = result.learning_opportunity.rating_data

        expect(rating_data['impact_lever'].present?).to be true
        expect(rating_data['impact_lever']['rating']).to eq score
        expect(rating_data['impact_lever']['reviews_count']).to eq review.review_impact_scores.count
        expect(rating_data['impact_levers'].present?).to be true
        expect(rating_data['impact_levers'].length).to eq review.review_impact_scores.uniq.count
      end
    end

    context 'with invalid params' do
      subject(:result) { described_class.call(learning_opportunity: nil) }

      it 'raises ArgumentError when LO is nil' do
        expect { result }.to raise_error(ArgumentError)
      end
    end
  end
end
